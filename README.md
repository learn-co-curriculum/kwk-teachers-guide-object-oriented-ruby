## Objectives

1. Students should review Ruby methods and conditionals
2. Students will learn about object orientation in Ruby and what OO means
3. Students should be able to define basic classes that can be initialized correctly

## Resources

* [Intro to OO Ruby](https://github.com/learn-co-curriculum/ruby-intro-to-oo)
* [Object Oriented Concepts](http://ruby.bastardsbook.com/chapters/oops/)
* [OO in Ruby](http://zetcode.com/lang/rubytutorial/oop/)
* [OO Basics](https://github.com/learn-co-curriculum/oo-basics)
* [OO Design Patterns](https://github.com/learn-co-curriculum/design-principles-readme)
* [Instantiating with #initialize](https://github.com/learn-co-curriculum/ruby-initialization)
